<?php

namespace App\Http\Controllers\Mail;

use App\Http\Controllers\Controller;
use Mail;
use Input;

class MailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }


    public function sendMail($template, $data)
    {

        Mail::send($template, $data, function ($message) use ($data) {
            /*$message->to('amatal28@gmail.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);*/
            $message->to('info@xpertosec.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);
            $message->to('ajimenez@xpertosec.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);
            $message->to('lchacon@xpertosec.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);
            $message->to('mrivera@xpertosec.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);
            $message->to('wgomez@xpertosec.com', $data['email'])->subject($data['subject'])->replyTo($data['replyTo'], $data['name']);
        });
    }
}
