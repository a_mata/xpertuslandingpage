/**
 * Created by Mata on 29/01/2016.
 */
var header = [], actual = 'white', color, scroll;
$(document).ready(function () {
    listenerAnimatedScroll();
    listenerWindowScroll();
    listenerWindowSize();
    listenerForm();
});

function listenerWindowScroll() {
    $(window).scroll(function () {
        scroll = jQuery(window).scrollTop();
        color = getHeaderColor(scroll);
        if (color != actual) {
            $('#headerLogo').addClass(actual);
            $('#headerLogo').removeClass(color);
            $('#topMenu').removeClass(actual + 'B');
            $('#topMenu').addClass(color + 'B');
            actual = color;
        }
    });
}

function listenerAnimatedScroll() {
    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1200);
    });
}

function listenerWindowSize() {
    loadHeaderRules();
    $(window).resize(function () {
        loadHeaderRules();
    });
}

function loadHeaderRules() {
    var last = 0;
    header = [];
    $('.content').each(function (index, value) {
        var offset = $(this).offset().top;
        if (offset > 0) {
            offset -= 50;
        }
        if (index % 2 == 0) {
            header.push({min: offset, color: 'white'});
        } else {
            header.push({min: offset, color: 'black'});
        }
        last = offset;
    });
    for (var i = 0; i < header.length - 1; i++) {
        header[i].max = header[i + 1].min;
    }
}

function getHeaderColor(scrolledFromtop) {
    for (var i = 0; i < header.length - 1; i++) {
        if (scrolledFromtop >= header[i].min && scrolledFromtop < header[i].max) {
            return header[i].color;
        }
    }
    return header[header.length - 1].color;
}

function listenerForm() {
    $("#contactForm").on('submit', function (ev) {
        ev.preventDefault();
        $('#loading').removeClass('hidden');
        $.ajax({
            type: 'POST',
            data: $("#contactForm").serialize(),
            url: SERVER + 'contact',
            success: function (data) {
                $('#loading').addClass('hidden');
                showMessage('success', 'Muchas Gracias!', 'pronto nos pondremos en contacto con usted', 6000);
                limpiarForm();
            },
            error: function (xhr, status, error) {
                $('#loading').addClass('hidden');
                limpiarForm();
                console.log(xhr + '-' + status + '-' + error);
            }
        });
    });
}

function limpiarForm() {
    $("#contactForm input,#contactForm textarea").val('');
}

function showMessage(type, strong, message, time) {
    $("#popMessage").removeClass('alert-success');
    $("#popMessage").removeClass('alert-danger');
    $("#popMessage").addClass('alert-' + type);
    $("#popMessage strong").html(strong);
    $("#popMessage span").html(message);
    $("#popMessage").css({
        'display': 'block'
    });
    setTimeout(function () {
        $("#popMessage").css({
            'display': 'none'
        });
    }, time);
}