@extends('template')
@section('title')
    Xpertos Executive Consulting
@stop
@section('content')
    <div class="alert fade in col-md-10 col-md-offset-1" id="popMessage">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>bla bla bla</strong>
        <span>bla bla bla bla</span>
    </div>
    <div class="col-sm-12 col-xs-12 hidden" id="loading">
        <div class="wrapper">
            <div class="loading p1"></div>
            <div class="loading p2"></div>
            <div class="loading p3"></div>
            <div class="loading p4"></div>
            <div class="loading p5"></div>
            <div class="loading p6"></div>
            <div class="loading p7"></div>
            <div class="loading p8"></div>
        </div>
    </div>
    @include('sections.home')
    @include('sections.services')
    @include('sections.experience')
    @include('sections.organization')
    @include('sections.contact')


@endsection