<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 col-md-offset-1">
                <div class="col-md-5 social">
                    <h5>Redes sociales: </h5>
                    <a href="https://www.facebook.com/Xpertos-Executive-Consulting-1694322540811956/" target="_blank"
                       class="socialLink">
                        <i class="fa fa-facebook"></i>
                    </a>
                </div>
                <div class="col-md-7">
                    <h5>
                        <i class="fa fa-copyright"></i>
                        Xpertos Executive Consulting. Todos los derechos Reservados. Costa Rica
                    </h5>
                </div>
            </div>
        </div>
    </div>
</footer>