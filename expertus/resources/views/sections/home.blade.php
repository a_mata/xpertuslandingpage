<section class="row format content" id="home">
    <div class="bgnSpace"></div>
    <div class="container slider_wrapper">
        <div class="col-md-6 col-sm-6" id="landingTitle">
            <img src="{{asset('theme/img/logo/logo_white.png')}}"/>
        </div>
        <div class="col-md-6 col-sm-6" id="valueProposal">
            <h1>Nuestra Propuesta de Valor</h1>
            <h2>Ofrecemos soluciones integrales a sus necesidades empresariales con base en nuestro conocimiento y
                experiencia en la gestión estratégica, administrativa, financiera, bancaria y tecnológica.</h2>
        </div>
    </div>
</section>
