<section class="row format content" id="experience">
    <div class="bgnSpace"></div>
    <div class="container">
        <div class="col-md-12 main">

        </div>
        <div class="col-md-6 col-sm-6">
            <div class="profile">
                <img src="{{asset('theme/img/services/adminstrative.png')}}"/>
                <h1>Mario Rivera</h1>
                <ul class=list>
                    <li>
                        <ul>
                            <li>
                                <span>37 años de experiencia en servicios bancarios y financieros</span>
                            </li>
                            <li>
                                <span>Del 2008 al 2014 fue el CEO (Gerente General) del Banco de Costa Rica (BCR), el segundo banco más grande en Costa Rica y el tercero en Centroamérica</span>
                            </li>
                            <li>
                                <span>Durante los últimos 25 años ocupó diversos puestos Gerenciales, hasta llegar a participar en las Juntas Directivas y Comités de Alto Nivel del Conglomerado BCR</span>
                            </li>
                            <li>
                                <span>Licenciado en Administración de Empresas
                                </span>
                            </li>
                            <li>
                                <span>MBA en Texas A&M International University</span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="profile">
                <img src="{{asset('theme/img/services/adminstrative.png')}}"/>
                <h1>Lissander Chacón</h1>
                <ul class=list>
                    <li>
                        <ul>
                            <li>
                                <span>28 años de experiencia en el sector financiero-bancario y en el sector empresarial</span>
                            </li>
                            <li><span>Ocupó en el Banco de Costa Rica los siguientes cargos ejecutivos: del 2009 al 2013 Director de Capital Humano, 2014 Director de Operaciones, 2015 Director de Banca Electrónica</span>
                            </li>
                            <li><span>Anteriormente se desempeñó en puestos gerenciales en el Banco de Costa Rica y a nivel de Centroamérica y el Caribe en la Asociación Internacional de Transporte Aéreo</span>
                            </li>
                            <li><span>Licenciado en Economía</span></li>
                            <li><span>Maestría en Administración de Empresas INCAE</span></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="profile">
                <img src="{{asset('theme/img/services/adminstrative.png')}}"/>
                <h1>William Gómez</h1>
                <ul class=list>
                    <li>
                        <ul>
                            <li><span>30 años de experiencia en el área de Tecnología de Información, 20 en el sector financiero-bancario empresarial</span>
                            </li>
                            <li><span>Se ha desempeñado como Director de Tecnología, Gerente de Banca Digital, Gerente de Desarrollo de Sistemas</span>
                            </li>
                            <li><span>Además cuenta con experiencia en Dirección de Proyectos, y consultoría en el sector financiero-tecnológico</span>
                            </li>
                            <li><span>Ha integrado Comités de Tecnología en el BCR y BICSA</span></li>
                            <li><span>Licenciado en Ingeniería de Sistemas</span></li>
                            <li><span>Maestría en Administración de Empresas INCAE</span></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="profile">
                <img src="{{asset('theme/img/services/adminstrative.png')}}"/>
                <h1>Alberto Jiménez</h1>
                <ul class=list>
                    <li>
                        <ul>
                            <li><span>34 años de experiencia en servicios bancarios y financieros estatales y privados, los últimos 26 en el Banco de Costa Rica (BCR)</span>
                            </li>
                            <li><span>Durante los últimos 20 años ocupó diversos puestos Gerenciales, hasta desempeñarse como Asistente de Gerencia General en los años 2010 a 2015</span>
                            </li>
                            <li><span>Ingeniero en Sistemas</span></li>
                            <li><span>Máster en Administración de Proyectos</span></li>
                            <li><span>Egresado del Programa de Alta Gerencia del INCAE</span></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>