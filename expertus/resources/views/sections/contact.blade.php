<section class="row format content" id="contact">
    <div class="bgnSpace"></div>
    <div class="container">
        <div class="col-md-12">
            <h1>Contáctenos</h1>


        </div>
        <div class="col-md-12">
            <form id="contactForm" role="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group col-md-6">
                    <label for="pwd">Nombre:</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                </div>
                <div class="form-group col-md-12">
                    <label for="email">Mensaje:</label>
                    <textarea type="email" class="form-control" id="message" name="message"
                              placeholder="Mensaje"></textarea>
                </div>
                <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-default pull-right">Enviar</button>
                </div>
            </form>


        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <h4>Para mas información comuníquese con nosotros: (506) 2268-1432</h4>
            </div>
        </div>
    </div>
</section>