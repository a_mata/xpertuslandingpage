<section class="row format content" id="services">
    <div class="bgnSpace"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Servicios</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4  col-sm-6 service">
                <img src="{{asset('theme/img/services/strategy.png')}}"/>
                <h1>Gesti&oacute;n Estrat&eacute;gica</h1>
                <ul>
                    <li>Planificación Estratégica</li>
                    <li>Planes comerciales y operativos</li>
                    <li>Gobierno Corporativo y Ética</li>
                    <li>Gestión de Proyectos</li>
                    <li>Cuadro de Mando Integral (BSC)</li>
                    <li>Net Promoter Score (NPS)</li>
                </ul>
            </div>
            <div class="col-md-4  col-sm-6 service">
                <img src="{{asset('theme/img/services/banks.png')}}"/>
                <h1>Gesti&oacute;n Bancaria</h1>
                <ul>
                    <li>Gestión Integral de Riesgo</li>
                    <li>Solicitud y Análisis de crédito</li>
                    <li>Formalización de crédito</li>
                    <li>Readecuaciones de crédito</li>
                    <li>Gestión de cobro</li>
                    <li>Recuperación de cartera</li>
                    <li>Manejo de inversiones</li>
                </ul>
            </div>
            <div class="col-md-4  col-sm-6 service">
                <img src="{{asset('theme/img/services/adminstrative.png')}}"/>
                <h1>Gestión Administrativa</h1>
                <ul>
                    <li>Gerencia y Coaching</li>
                    <li>Procesos y Logística</li>
                    <li>Operaciones</li>
                    <li>Capital Humano</li>
                    <li>Mercadeo</li>
                    <li>Contratación Administrativa</li>
                    <li>Capacitación</li>
                </ul>
            </div>
            <div class="col-md-4 col-md-offset-2 col-sm-6 service">
                <img src="{{asset('theme/img/services/finances.png')}}"/>
                <h1>Gestión Financiera</h1>
                <ul>
                    <li>Presupuesto</li>
                    <li>Análisis y valoración financiera</li>
                    <li>Restructuraciones financieras</li>
                    <li>Fusiones y adquisiciones</li>
                </ul>
            </div>
            <div class="col-md-4  col-sm-6 service">
                <img src="{{asset('theme/img/services/tech.png')}}"/>
                <h1>Gesti&oacute;n Tecnol&oacute;gica</h1>
                <ul>
                    <li>Análisis integral de la plataforma tecnológica</li>
                    <li>Gobierno de TI</li>
                    <li>Administración de Proyectos</li>
                    <li>Análisis organizacional de TI</li>
                    <li>Análisis oferta y demanda de TI</li>
                    <li>Arquitectura Tecnológica</li>
                    <li>Continuidad del Negocio / Contingencia</li>
                </ul>
            </div>
        </div>

    </div>
</section>