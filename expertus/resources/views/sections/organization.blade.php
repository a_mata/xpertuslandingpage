<section class="row format content" id="organization">
    <div class="bgnSpace"></div>
    <div class="container">
        <div class="col-md-12">
            <h1>Organizaci&oacute;n </h1>
        </div>


        <div class="col-md-6">
            <h2>Nuestros Valores</h2>
            <ul>
                <li>Compromiso</li>
                <li>Transparencia</li>
                <li>Discreción</li>
                <li>Disciplina</li>
                <li>Servicio al Cliente</li>
                <li>Disponibilidad al Cambio</li>
            </ul>


        </div>
        <div class="col-md-6">
            <h2>Misi&oacute;n</h2>
            <h4>Brindar asesoría de manera integral en las diferentes áreas de su empresa, aportando alto valor agregado
                para la toma de decisiones.</h4>
            <h2>Visi&oacute;n</h2>
            <h4>Ser reconocidos como una empresa que construye relaciones estables y duraderas con nuestros
                clientes.</h4>
        </div>

    </div>
</section>