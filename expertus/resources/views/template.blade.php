<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('theme/css/bootstrap/bootstrap.min.css')}}" type="text/css" rel="stylesheet">
    <link rel="icon" type="image/ico" href="{{ asset('theme/img/logo/favicon.png') }}"/>
    <link href="{{asset('theme/css/style.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('theme/css/medias.css')}}" type="text/css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<header>
    <nav id="topMenu" class="navbar navbar-default navbar-fixed-top clearfix">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#home" class="scroll hm">
                    <div id="headerLogo" class="black"></div>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul data-toggle="collapse" data-target="#navbar" class="nav navbar-nav navbar-right">
                    <li><a href="#home" class="scroll hm">Inicio</a></li>
                    <li><a href="#services" class="scroll srv">Servicios</a></li>
                    <li><a href="#experience" class="scroll prods">Experiencia</a></li>
                    <li><a href="#organization" class="scroll">Organización</a></li>
                    <li><a href="#contact" class="scroll cntc">Contacto</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</header>
<div class="container-fluid" id="app">
    @yield('content')
</div>
@include('footer')
<script>
    var SERVER = '{{ url('api/') }}/';
</script>
<script src="{{asset('theme/js/jquery.js')}}"></script>
<script src="{{asset('theme/js/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('theme/js/script.js')}}"></script>
</body>

</html>

